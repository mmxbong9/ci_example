﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "ExamCharacter.h"

AExamCharacter::AExamCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

}

void AExamCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AExamCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExamCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}
